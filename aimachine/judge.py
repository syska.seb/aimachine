import numpy as np

from aimachine.board import Board
from aimachine.turnresult import TurnResult


class Judge:
    _MIN_TURNS_COUNT = 5
    _MAX_TURNS_COUNT = 9

    def announce_turn_result(self, board: Board, turn_number: int) -> TurnResult:
        board_values: np.ndarray = board.get_all_field_values()

        if turn_number < self._MIN_TURNS_COUNT:
            return TurnResult.GAME_ONGOING

        if self._have_same_values_orthogonally(board_values) or self._have_same_values_diagonally(board_values):
            return TurnResult.WIN

        if turn_number == self._MAX_TURNS_COUNT:
            return TurnResult.TIE

        return TurnResult.GAME_ONGOING

    def _have_same_values_orthogonally(self, board_values: np.ndarray) -> bool:
        for i in range(0, 3):
            if self._get_absolute_sum(board_values[i, :]) == 3 or self._get_absolute_sum(board_values[:, i]) == 3:
                return True

        return False

    def _have_same_values_diagonally(self, board_values: np.ndarray) -> bool:
        diagonal_values = np.diagonal(board_values)
        opposite_diagonal_values = np.diagonal(np.fliplr(board_values))
        if self._get_absolute_sum(diagonal_values) == 3 or self._get_absolute_sum(opposite_diagonal_values) == 3:
            return True

        return False

    @staticmethod
    def _get_absolute_sum(values: np.ndarray) -> int:
        return abs(sum(values))
