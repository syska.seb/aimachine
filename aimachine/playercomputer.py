import pickle
from typing import List, Dict, Tuple

import numpy as np

from aimachine.board import Board
from aimachine.player import Player
from aimachine.symbol import Symbol


class PlayerComputer(Player):
    _DEFAULT_MIN_STATE_VALUE = -999.0

    def __init__(
            self,
            name: str,
            symbol: Symbol,
            learning_rate=0.2,
            exploration_rate=0.3,
            gamma=0.9
    ):
        Player.__init__(self, name, symbol)
        self._learning_rate = learning_rate
        self._exploration_rate = exploration_rate
        self._gamma = gamma
        self._archived_field_values: List[str] = []
        self._state_values: Dict[str, float] = {}

    def make_move(self, board: Board, row_index, col_index) -> Board:
        available_field_indices = board.get_available_field_indices()
        row_index, col_index = self._choose_move_indices(board, available_field_indices)
        board.set_field_value(row_index, col_index, self.symbol.value.token)
        self._store_field_values_hash(board)
        return board

    def _choose_move_indices(self, board: Board, available_field_indices: List[Tuple[int, int]]) -> Tuple[int, int]:
        if np.random.uniform(0.0, 1.0) <= self._exploration_rate:
            return self._get_random_field_indices(available_field_indices)
        else:
            board_values = board.get_all_field_values()
            return self._get_computed_field_indices(board_values, available_field_indices)

    @staticmethod
    def _get_random_field_indices(available_field_indices: List[Tuple[int, int]]) -> Tuple[int, int]:
        choice = np.random.choice(len(available_field_indices))
        row_index = available_field_indices[choice][0]
        col_index = available_field_indices[choice][1]
        return row_index, col_index

    def _get_computed_field_indices(
            self,
            board_values: np.ndarray,
            available_field_indices: List[Tuple[int, int]]
    ) -> Tuple[int, int]:
        min_state_value = self._DEFAULT_MIN_STATE_VALUE
        row_index = available_field_indices[0][0]
        col_index = available_field_indices[0][1]
        for indices_pair in available_field_indices:
            row = indices_pair[0]
            col = indices_pair[1]
            next_field_values = board_values.copy()
            next_field_values[row, col] = self.symbol.value.token
            next_field_values_hashed = Board.get_field_values_hash(next_field_values)
            next_state_value = self._state_values.get(next_field_values_hashed)
            state_value = 0.0 if next_state_value is None else next_state_value
            if state_value >= min_state_value:
                min_state_value = state_value
                row_index = row
                col_index = col
        return row_index, col_index

    def _store_field_values_hash(self, board: Board):
        board_field_values_hashed = Board.get_field_values_hash(board.get_all_field_values())
        self._archived_field_values.append(board_field_values_hashed)

    def _get_reward_difference_for_next_step(self, field_values: str, reward: float) -> float:
        return self._learning_rate * (self._gamma * reward - self._state_values[field_values])

    def feed_reward(self, reward: float):
        for field_values in reversed(self._archived_field_values):
            if self._state_values.get(field_values) is None:
                self._state_values[field_values] = 0
            else:
                self._state_values[field_values] += self._get_reward_difference_for_next_step(field_values, reward)
            reward = self._state_values[field_values]

    def save_policy(self):
        writer = open('policy_' + str(self.symbol.name), 'wb')
        pickle.dump(self._state_values, writer)
        writer.close()

    def load_policy(self, file):
        reader = open(file, 'rb')
        self._state_values = pickle.load(reader)
        reader.close()

    def clear_archived_field_values(self):
        self._archived_field_values = []
