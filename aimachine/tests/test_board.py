from unittest import TestCase

import numpy as np

from aimachine.board import Board


class TestBoard(TestCase):

    def test_init__no_value_passed__all_field_values_blanks(self):
        board = Board()
        self.assertTrue(all(v == Board.BLANK_VALUE for v in board.get_all_field_values().reshape(9)))

    def test_set_field_value(self):
        board = Board()
        field_row = 1
        field_col = 2
        field_value = 44
        self.assertFalse(board.get_all_field_values().__contains__(field_value))
        board.set_field_value(field_row, field_col, field_value)
        self.assertTrue(board.get_all_field_values().__contains__(field_value))

    def test_is_field_available(self):
        board = Board()
        field_row = 2
        field_col = 0
        field_value = 7
        self.assertTrue(board.is_field_available(field_row, field_col))
        board.set_field_value(field_row, field_col, field_value)
        self.assertFalse(board.is_field_available(field_row, field_col))

    def test_get_available_field_indices(self):
        board = Board()
        field_row = 2
        field_col = 0
        field_value = 7
        self.assertTrue(board.get_available_field_indices().__contains__((field_row, field_col)))
        board.set_field_value(field_row, field_col, field_value)
        self.assertFalse(board.get_available_field_indices().__contains__((field_row, field_col)))

    def test_clear_fields(self):
        board = Board()
        empty_board = Board(Board.BLANK_VALUE * np.zeros((3, 3), int))
        self.assertEqual(board.get_all_field_values().tolist(), empty_board.get_all_field_values().tolist())
        board.set_field_value(1, 1, 7)
        board.set_field_value(0, 2, -4)
        self.assertNotEqual(board.get_all_field_values().tolist(), empty_board.get_all_field_values().tolist())
        result = board.clear_all_fields()
        self.assertEqual(board.get_all_field_values().tolist(), result.get_all_field_values().tolist())
