from unittest import TestCase

import numpy as np

from aimachine.board import Board
from aimachine.judge import Judge
from aimachine.turnresult import TurnResult


class TestJudge(TestCase):

    def test_announce_turn_result__ninth_turn__no_result__returns_tie(self):
        board_field_values = np.ndarray((3, 3), int)
        board_field_values[:] = [[1, -1, 1],
                                 [1, -1, 1],
                                 [-1, 1, -1]]
        board = Board(board_field_values)
        judge = Judge()
        self.assertEqual(TurnResult.TIE, judge.announce_turn_result(board, 9))

    def test_announce_turn_result__fourth_turn__returns_game_ongoing(self):
        board_field_values = np.ndarray((3, 3), int)
        board_field_values[:] = [[1, 1, 1],
                                 [1, 1, 1],
                                 [1, 1, 1]]
        board = Board(board_field_values)
        judge = Judge()
        self.assertEqual(TurnResult.GAME_ONGOING, judge.announce_turn_result(board, 4))

    def test_announce_turn_result__x_begins__all_diagonal_x__returns_win(self):
        board_field_values = np.ndarray((3, 3), int)
        board_field_values[:] = [[1, -1, 1],
                                 [-1, 1, -1],
                                 [0, 0, 1]]
        board = Board(board_field_values)
        judge = Judge()
        self.assertEqual(TurnResult.WIN, judge.announce_turn_result(board, 6))

    def test_announce_turn_result__o_begins__all_opposite_diagonal_o__returns_win(self):
        board_field_values = np.ndarray((3, 3), int)
        board_field_values[:] = [[1, -1, -1],
                                 [1, -1, 0],
                                 [-1, 0, 1]]
        board = Board(board_field_values)
        judge = Judge()
        self.assertEqual(TurnResult.WIN, judge.announce_turn_result(board, 5))

    def test_announce_turn_result__o_begins__all_orthogonal_x__returns_win(self):
        board_field_values = np.ndarray((3, 3), int)
        board_field_values[:] = [[-1, 1, -1],
                                 [1, 1, 1],
                                 [0, -1, 1]]
        board = Board(board_field_values)
        judge = Judge()
        self.assertEqual(TurnResult.WIN, judge.announce_turn_result(board, 9))
