from aimachine.board import Board
from aimachine.symbol import Symbol


class Player:
    def __init__(self, name: str, symbol: Symbol):
        self.name = name
        self.symbol = symbol

    def make_move(self, board: Board, row_index, col_index) -> Board:
        pass

    def feed_reward(self, reward: float):
        pass

    def clear_archived_field_values(self):
        pass

    def save_policy(self):
        pass
