from enum import Enum


class TurnResult(Enum):
    GAME_ONGOING = 1
    WIN = 2
    TIE = 3
