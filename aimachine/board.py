from typing import List, Tuple

import numpy as np


class Board:
    BLANK_VALUE: int = 0

    def __init__(self, all_field_values: np.ndarray = None):
        if all_field_values is not None:
            self._all_field_values = all_field_values
        else:
            self._all_field_values = Board.BLANK_VALUE * np.zeros((3, 3), int)

    def get_all_field_values(self) -> np.ndarray:
        return self._all_field_values

    def get_available_field_indices(self) -> List[Tuple[int, int]]:
        row_indices, col_indices = np.where(self._all_field_values == self.BLANK_VALUE)
        return list(zip(row_indices, col_indices))

    def set_field_value(self, row_index: int, col_index: int, field_value: int):
        self._all_field_values[row_index, col_index] = field_value

    def is_field_available(self, row_index, col_index) -> bool:
        return self._all_field_values[row_index, col_index] == Board.BLANK_VALUE

    def clear_all_fields(self):
        self._all_field_values.fill(self.BLANK_VALUE)
        return self

    @staticmethod
    def get_field_values_hash(field_values: np.ndarray) -> str:
        return str(field_values.reshape(9))
