from aimachine.board import Board
from aimachine.player import Player


class PlayerHuman(Player):

    def make_move(self, board: Board, row_index, col_index) -> Board:
        board.set_field_value(row_index, col_index, self.symbol.value.token)
        return board
