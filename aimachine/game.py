from typing import List

import flask_socketio

from aimachine.board import Board
from aimachine.judge import Judge
from aimachine.player import Player

from aimachine.playerhuman import PlayerHuman
from aimachine.symbol import Symbol
from aimachine.turnresult import TurnResult


class Game:
    _PLAYER_STUB = Player("", Symbol.SYMBOL_X)

    def __init__(self, game_id: str):
        self._game_id = game_id
        self._board = Board()
        self._judge = Judge()
        self._player_1 = Game._PLAYER_STUB
        self._player_2 = Game._PLAYER_STUB
        self._current_player = Game._PLAYER_STUB
        self._turn_result = TurnResult.GAME_ONGOING
        self._turn_number = 0
        self._player_ids: List[str] = []

    def on_player_joined_game(self, sid: str):
        flask_socketio.emit("client_id", sid)
        if self._player_1 is Game._PLAYER_STUB:
            self._player_ids.append(sid)
            self._player_1 = PlayerHuman(sid, Symbol.SYMBOL_O)
            self._current_player = self._player_1
        else:
            self._player_ids.append(sid)
            self._player_2 = PlayerHuman(sid, Symbol.SYMBOL_X)
            flask_socketio.emit("movement_allowed", self._current_player.name, room=self._game_id)
        print("player: {} joined the game: {}".format(sid, self._game_id))
        flask_socketio.emit("server_message", "players in game: {}".format(self._player_ids), room=self._game_id)

    def on_field_clicked(self, row_index: int, col_index: int):
        if self._board.is_field_available(row_index, col_index):
            print("clicked [row, col]: [{}, {}]".format(row_index, col_index))
            data_to_send = {"rowIndex": row_index, "colIndex": col_index,
                            "fieldToken": self._current_player.symbol.value.token}
            flask_socketio.emit("field_to_be_marked", data_to_send, room=self._game_id)
            if self._turn_result == TurnResult.GAME_ONGOING:
                self._turn_number += 1
                self._board = self._current_player.make_move(self._board, row_index, col_index)
                self._turn_result = self._judge.announce_turn_result(self._board, self._turn_number)
                if self._turn_result == TurnResult.GAME_ONGOING:
                    self._change_player()
                else:
                    result_message = self._get_result_message()
                    flask_socketio.emit("server_message", "game ended: {} ".format(result_message),
                                        room=self._game_id)
                    flask_socketio.emit("movement_allowed", "none", room=self._game_id)

    def _change_player(self):
        self._current_player = self._player_2 if self._current_player == self._player_1 else self._player_1
        flask_socketio.emit("movement_allowed", self._current_player.name, room=self._game_id)

    def _get_result_message(self) -> str:
        if self._turn_result is TurnResult.TIE:
            return "Tie"
        else:
            return self._current_player.symbol.value.identifier + " won"

    def on_disconnect(self, sid: str):
        self._player_ids.remove(sid)
        print("player: {} disconnected from game: {}".format(sid, self._game_id))
        flask_socketio.emit("server_message", "player: {} disconnected".format(sid),
                            room=self._game_id)
        flask_socketio.emit("server_message", "players in game: {}".format(self._player_ids), room=self._game_id)
