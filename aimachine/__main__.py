from typing import Dict

import flask
import flask_socketio

from aimachine.game import Game

APP = flask.Flask(__name__)
SOCKET_IO = flask_socketio.SocketIO(APP)
GAMES: Dict[str, Game] = {}
ROOMS_COUNTER = 1


@APP.route("/")
def start_homepage():
    return flask.render_template("index.html")


@SOCKET_IO.on("connect")
def on_client_connected():
    sid = flask.request.sid
    global ROOMS_COUNTER, GAMES
    game_id = "game" + str(ROOMS_COUNTER)
    flask_socketio.emit("game_id", game_id)
    if game_id not in GAMES.keys():
        flask_socketio.join_room(game_id, sid)
        game = Game(game_id)
        game.on_player_joined_game(sid)
        GAMES[game_id] = game
        print("games: {}".format(list(GAMES.keys())))
    else:
        flask_socketio.join_room(game_id, sid)
        GAMES[game_id].on_player_joined_game(sid)
        ROOMS_COUNTER = ROOMS_COUNTER + 1


@SOCKET_IO.on("field_clicked")
def on_field_clicked(data):
    game_name = data["gameId"]
    row_index = data["rowIndex"]
    col_index = data["colIndex"]
    GAMES[game_name].on_field_clicked(row_index, col_index)


@SOCKET_IO.on("disconnect")
def on_client_disconnected():
    sid = flask.request.sid
    game_id = flask_socketio.rooms(sid)[1]
    try:
        server_message = "Game: {} has been disbanded, restart client to play a new game".format(game_id)
        SOCKET_IO.emit("server_message", server_message, room=game_id)
        GAMES[game_id].on_disconnect(sid)
        flask_socketio.close_room(game_id)
        print("Games: {}".format(list(GAMES.keys())))
        del GAMES[game_id]
        print("Games: {}".format(list(GAMES.keys())))
    except KeyError:
        pass


@SOCKET_IO.on_error()
def on_error_handler():
    sid = flask.request.sid
    game_id = flask_socketio.rooms(sid)[1]
    request = flask.request
    print("server error")
    print(request.event["message"])
    print(request.event["args"])
    flask_socketio.emit("server_message", "server error", room=game_id)


if __name__ == "__main__":
    SOCKET_IO.run(APP, host="0.0.0.0", debug=True)
